const express = require('express');
const app = express();
const port = 3000;

const version = '1.0.0';

app.get('/', (req, res) => {
  const message = `<div style="display: flex; justify-content: center; align-items: center; height: 100vh;">
                    <h1>Hello, World!</h1>
                    <h2 style="margin-left: 20px; color: orange;">Version: ${version}</h2>
                  </div>`;
  res.send(message);
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
